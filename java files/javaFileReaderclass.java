/* 
We can use FileReader class object to read character data from the file.

constructors:
1. FileReader fr = new FileReader(String fname);
2. FileReader fr = new FileReader(File f);


methods:
1. int read();
	e.g. int i = fr.read();
i) It attempts to read next character from the file and returns its unicode value.
ii) If there is no next character present in file then it will return -1.
iii) As this method returns unicode value compulsory at the time of printing compulsory we should perform type casting. 

2. int read(char[]  ch);
It attempts to read enough characters from the file into char[] and returns the number of characters copied from the File into char[].

3. void close(); ---- To close Reader.

A demo program to demonstrate use of FileReader.
*/

import  java.io.*;

public  class javaFileReaderclass
{
	public static void main(String[] args) throws IOException
	{
		File f = new File("paper.txt");
		FileReader fr = new FileReader(f);

		/*int i = fr.read();

		while (i != -1) 
		{
			This is 1st approach.
			System.out.print((char)i);
			 i = fr.read();
		}*/

		char[] ch  = new char [(int)f.length()]; //char[]  ch = new char[xxxxxx];  Here the size of character array is not determine by programmer but it is determine on the basis of characters present in the file.
		//In the above line we are performing  type casting otherwise we will get compile time error. 
		
		fr.read(ch);//This line is reading and copying the content of file paper.txt character by character. 

		for (char ch1 : ch ) //for each char ch in ch1.
		{
			System.out.print(ch1);
		}
		fr.close();
	}
}

/* 
Note :- 
In java the maximum allowed array size is int but the return type of length() method is long and therefore long size array is 
not allowed in java.

Limitations of FileReader
By using FileReader class we can read data character by character but not line by line which is very inconvenient to the programmer.
*/
