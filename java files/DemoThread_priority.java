/*A demo program to get and set Thread priority.
A Thread class defines the following 2 methods to get and set priority of Thread
1. public final int getPriority();
2. public final void setPriority(int p); ---  allowed values range is 1 to 10.

*/

class mythread extends Thread
{

}

public  class DemoThread_priority
{
	public static void main(String[] args) 
	{
		//Thread.currentThread().setPriority(9);
		System.out.println("priority of main Thread = " + Thread.currentThread().getPriority() );// The default priority for main Thread is 5.

	
		//Thread.currentThread().setPriority(15);// Exception in thread "main" java.lang.IllegalArgumentException

		mythread t = new mythread();

		System.out.println("priority of child Thread = " + t.getPriority() );
		// This above line is equivalent to Thread.currentThread().getPriority(); bcoz t is of type mythread class which extends Thread class
		// Therefore Thread class method is available to its child class i.e by inheritance concept

		// The default priority for child Thread will be inherited from its parent Thread
		// i.e. whatever priority parent Thread has, the same priority will be there for child Thread.

		
	}
}