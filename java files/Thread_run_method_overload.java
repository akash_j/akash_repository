//program to demonstrate overloading of run() method


class Mythreaddemo extends Thread
{
	public void run()
	{
		System.out.println("no- argument run() method executed");
	}

	public void run(String i)
	{
		System.out.println(i);
	}
}

public class Thread_run_method_overload
{
	public static void main(String[] args) 
	{
		Mythreaddemo t = new Mythreaddemo();

					 t.start();

					 t.run("hello world");

	}
}

/*In the above the t.start() method will always invoke no- argument run() method.
The overloaded run() method, we have to call it explicity.*/