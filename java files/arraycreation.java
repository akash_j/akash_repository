/* 

Array creation 

	e.g. int[]   x = new int[3];

Every Array in java is an object hence we create arrays by using new operator.
	int[] 	x = new int[]; compile time error.

	int[] ---->  Array  declaration
	new	 int[]-----> Array creation

case 1:

At the time of Array creation compulsory we should specify the array size otherwise we will get compile time error.
	e.g. int[]  x = new int[5];  ----  perfectly valid	

case  2:
	It is legal to  have an array with size 0 in java.  
		e.g. int[]  x = new int[0]; ---  perfectly valid.

case 3 :
	If we are trying to specify array size with negative int values then we will get compile time error. 
	e.g. int[]  x = new int[-3]; ---- invalid
		RuntimeException : NegativeArraySizeException

case 4: 
		To specify Array size the allowed data types are
		1. byte
		2. short
		3. char
		4. int

		If we are trying to specify any other type then we will get compile time error.
		