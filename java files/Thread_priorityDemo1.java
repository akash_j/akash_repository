class mythread extends Thread
{
	public  void run()
	{
		for (int i = 1; i<=10 ; i++ ) 
		{
			System.out.println("child Thread");
		}
	}
}

public  class Thread_priorityDemo1
{
	public static void main(String[] args) 
	{
		mythread t  = new mythread();

		//t.setPriority(9);

		t.start();

		for (int i=1; i<=10 ; i++) 
		{
			System.out.println("main Thread");
		}
	}
}