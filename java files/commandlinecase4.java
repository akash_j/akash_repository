/* usually space itself is the separator between command line arguments. If our command line arguments itself contains 
space then we have to enclose that command line argument within double quotes.

case 4: 

*/

public class commandlinecase4 
{
	public static void main(String[] args) 
	{
		System.out.println(args[0]);
	}
}	


/* o/p

java commandlinecase4 Hello World
Hello
java commandlinecase4 "Hello World"
Hello World
*/


