/* If we are not overriding  run() method then Thread class run() method will
be executed which has empty implementation. Hence, we won't get any output*/

// e.g Demo

class Mythread101 extends Thread
{
	// here we are not overriding i.e. implementing run() method of Thread class
}

public  class not_overriding_run_method
{
	public static void main(String[] args) 
	{
		Mythread101 t  = new Mythread101();

					t.start();
					
	}
}

