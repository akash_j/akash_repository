/* We can prevent a Thread execution by following 3 methods
1. yield() ---> It causes pause to current executing thread to give the chance for remaining waiting threads of same priority.
whenever yield() method is invoked the thread enters into ready/runnable state.
prototype of yield() method ----> public static native void yield();

2. join()
3. sleep()

*/

class mythread extends Thread
{
	public void run()
	{
		for (int i=1; i<=10 ; i++ ) 
		{
			System.out.println("child Thread");
			Thread.yield();	
		}

	}
}

public  class yieldmethod
{
	public static void main(String[] args) 
	{
		mythread t = new mythread();

		t.start();

		for (int i=1; i<=10 ;i++ ) 
		{
			System.out.println("main Thread");
		}

		
	}
}

//note:- some platforms won't provide support for yield() method.
