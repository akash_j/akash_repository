/*
we can use BufferedReader to read character data (text data) from the file.
The main advantage of BufferedReader over FileReader is that we can read data line by line in addition to character by character.
It is therefore much convenient to the programmer.

constructors:
1. BufferedReader br = new BufferedReader(Reader r);
2. BufferedReader br = new BufferedReader(Reader r, int buffersize);

methods of BufferedReader:
1.int read();
	
2. int read(char[]  ch);

3. void close(); ---- To close reader.

4. String readLine(); 
		e.g. String line = br.readLine(); 
It attempts to read next line from the file and returns it if it is available.
If the next line is not available then it returns null.

Demo program to demonstrate use of BufferedReader
*/
import java.io.*;

public  class javaBufferedReader
{
	public static void main(String[] args) throws Exception 
	{
		FileReader fr = new FileReader("akon.txt");

		BufferedReader br = new BufferedReader(fr);

		String line = br.readLine(); // reads a single line nad stores in line variable.

		while (line != null) // line != null means valid data. 
		{
			System.out.println(line);
			line = br.readLine();
		}
		//The above loop will break if there is no next line available in file.
		br.close();
	}
}
//BufferedReader is the most enhanced reader to read character data from the file.