
// This program illustrates how to create file in java 
import java.io.*;
public class createfiledemo
{
	public static void main(String[] args) throws Exception
	 {
		File f = new File ("abc.txt"); // This line won't create any  new physical file. 
		//line no 8 and 13 both these lines are the syntax to create new file

		System.out.println(f.exists()); // returns false

		f.createNewFile(); // This line creates new file named with abc.txt

		System.out.println(f.exists());// returns true

		System.out.println(f.isFile());// returns true if file object f is pointing to physical file 
	}
}