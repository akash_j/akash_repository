// Within main method command line arguments are available in String form

//case 3:

public class commandlinecase3
{
	public static void main(String[] args) 
	{
		System.out.println(args[0] + args[1]);
	} 
}

/*  
o/p
java commandlinecase3 10 20
1020
*/
