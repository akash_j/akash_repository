/*A  thread can interrupt a sleeping thread or waiting thread by using interrupt() method of Thread class

prototype --->  public  void interrupt()

A program to demonstrate use of interrupt() method
*/

class mythread extends Thread
{
	public void run()
	{
		
		try
		{
			for (int i=1; i<=10 ; i++ ) 
			{
				System.out.println("I am lazy Thread");
				Thread.sleep(2000);//InterruptedException occurs here if t.interrupt() method is invoked and then control is transfered to catch block.
			}
		}

		catch (Exception e) 
		{
			System.out.println("I got interrupted");
		}
	}
}

public  class interruptmethod
{
	public static void main(String[] args) 	
	{
		mythread t = new mythread();

		t.start();
		t.interrupt();//here main Thread is calling interrupt() method on child Thread object hence main thread interrupts child thread.

		System.out.println("End of main Thread");



	}
}