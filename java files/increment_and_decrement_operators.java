class increment_and_decrement_operators
{
	public static void main(String[] args) {
		//illustration  of post increment and pre increment operators 

		int count = 5;

		/*int p = count++;*/     // This is post increment and this is punch line of code 
		//breaking the above statment into 2 parts
		// 1. p = count 
		// 2. count = count++ 
		// post increment is nothing but first assigning then increment
		 

		 int p = ++count;  //This is pre  increment  illustration 
		 //breaking the above statement into 2 parts
		 //1. count = count + 1
		 //2. p = count   
		 //pre increment is nothing but  first increment then assigning

		 System.out.println("P = " + p);
		 System.out.println("count = " + count); 


	}
}