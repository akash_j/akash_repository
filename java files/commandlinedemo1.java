/* 
1. The arguments which are passed from comand prompt are called commandline argumnts.

2. with these command line arguments jvm will create an array and by passing that array as argument jvm will
call main method.

3. The main purpose of commad line arguments is we can customize behaviour of main method.

4. String is the most commonly used object therefore SUN people gave most priority to String.

A program to demonstrate comman line arguments
*/
// case 1:
public  class  commandlinedemo1
{
	public static void main(String[] args) 
	{
		for (int i=0; i<=args.length ; i++ ) // if we replace <= with < then we won't get any runtime exception
		{
			System.out.println(args[i]);
		}
	}
}

/* o/p 
 java commandlinedemo1 A B C
A
B
C
Exception in thread "main" java.lang.ArrayIndexOutOfBoundsException: 3
*/