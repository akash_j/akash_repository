/*
PrintWriter class
1. It is the most enhanced writer to write character data to the file.
2. The main advantage of PrintWriter is that we can write any type of primitive data directly to the file.
3. It is the most powerful writer wherever writing data is required.

constructors:
1. PrintWriter pw = new PrintWriter(String name);
2. PrintWriter pw = new PrintWriter(File f);
3. PrintWriter pw = new PrintWriter(Writer w);

Note:- PrintWriter can communicate directly with the file or via some writer object also.

*write methods of PrintWriter class
1. write(int num) :- To write single character to the file. 
	int ch is treated as unicode value.

2. write(char[] ch) --- To write an array of characters to the file.

3. write(String s) --- To write a String to the file.

4. flush() --- To give guarantee that total data including last character written properly to the file.This is very important method.

5. close() -- To close a FileWriter.

*Print methods of PrintWriter class
1. print(char ch);
2. print(int i);
3. print(double d);
4. print(boolean b);
5. print(String s);

*println methods of PrintWriter class
1. println(char ch);
2. println(int i);
3. println(double d);
4. println(boolean b);
5. println(String s);

A demo programme to demonstrate PrintWriter class 
*/
import  java.io.*;
public  class javaPrintWriter
{
	public static void main(String[] args) throws IOException
	{
		FileWriter fw = new FileWriter("abc.txt", true);
		PrintWriter pw = new PrintWriter(fw);
		pw.println();
		
		pw.println("hello to programming world");

		pw.println(true);

		pw.println(10.5);

		pw.println(-1452);

		pw.println('a');

		pw.write(215);

		pw.flush();

		pw.close();
	}
}

