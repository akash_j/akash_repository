// It is not recommended to override start() method of Thread class otherwise  don't go for multithreading 

class Mythread extends Thread
{
	public void start()
	{
		super.start();
		System.out.println("start method");
	}

	public void run()
	{
		System.out.println("run method");
	}
}

public class startexample
{
	public static void main(String[] args) 
	{
		Mythread t  = new Mythread();

				 t.start();

				 System.out.println("main method"); 

	}
}