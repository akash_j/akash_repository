//Program to  print names of all Files and sub-directories present is specified directory.


import  java.io.*;

public class fileioprogram2
{
	public static void main(String[] args) throws Exception
	{
		int count = 0 ; 

		File  f = new File ("/home/ajay123/Desktop/java files");

		String[] s = f.list();// This method returns the name of  all files and dirctories present in specified directory 


		for (String s1:s)// For each string s1 in s. It is a for each loop
		{
			count++;

			System.out.println(s1);
		    
		}
		System.out.println("The total no: " + count);
		
	}
}
/* Important methods of file class 

1. boolean exists() --- returns true whether the specified physical file or directory exists in our system.

2. boolean createNewFile() ---  First this method will check whether the physical file is already available or not. 
If it is already available then this method returns false without creating any physical file. 
If it is not already available then this method creates new file and returns true.

3. boolean mkdir()  ----- First this method will check whether the physical directory is already available or not. 
If it is already available then this method returns false without creating any physical directory.
If it is not already available then this method creates new directory and returns true.

4. boolean isFile() ---- returns true if file object is pointing to physical file.

5. boolean isDirectory() ---  returns true if file object is pointing to physical directory.

6.String[]  list()  ---- returns the name of all files and directories present is specified directory.
	e.g. String[]   s = f.list();

7. long length() ---  returns the number of characters present in specified file.
	e.g. long l = f.length(); 

boolean delete() ---- It is used to delete specified file or directory.


File class constructors
1.File f = new File(String name) --- creates a java file object to represent name of specified file or directory present in 
current working directory.

2. File f = new File (String subdir, String name)