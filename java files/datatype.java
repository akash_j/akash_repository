// A prgram for default values of data types of java.

public  class datatype
{
	static 	byte	b;
	
	static 	short	st;
	
	static 	char	ch;
	
	static 	int		i;
	
	static 	float	f;
	
	static 	boolean	boo;
	
	static 	double	d;
	
	static 	long	l;
	
	static 	String	s;



	public static void main(String[] args) 
	{	
		
		
		System.out.println("Default value of byte is: " + b);

		System.out.println("Default value of short is: " + st);

		System.out.println("Default value of char is: " + ch);

		System.out.println("Default value of int is: " + i);

		System.out.println("Default value of float is: " + f);

		System.out.println("Default value of boolean is: " + boo);

		System.out.println("Default value of double is: " + d);

		System.out.println("Default value of long is: " + l);

		System.out.println("Default value of String is: " + s);

	}
}

/*
output 
Default value of byte is: 0
Default value of short is: 0
Default value of char is: 
Default value of int is: 0
Default value of float is: 0.0
Default value of boolean is: false
Default value of double is: 0.0
Default value of long is: 0
Default value of String is: null
*/


// Note:- null is the default value for object reference.
	 