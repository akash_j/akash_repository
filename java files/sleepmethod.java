/* If a thread don't want to perform any operation for a particular amount of time then we should go for sleep() method.

prototype of sleep() method 
1. public  static native void  sleep(long ms) throws InterruptedException 
2. public static  void sleep(long ms, int ns) throws InterruptedException

A program to demonstrate use of sleep()  method 
*/

public  class sleepmethod
{
	public static void main(String[] args) throws InterruptedException
	{
		for (int i=1; i<=10 ; i++ ) 
		{
			System.out.println("sleeping..... " + i);
			Thread.sleep(2000);

		}
	}
}