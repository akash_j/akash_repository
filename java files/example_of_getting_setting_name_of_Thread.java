class mythread extends Thread
{
	public  void run()
	{
		System.out.println("run() method executed by " + Thread.currentThread().getName() );
	}
}

public  class example_of_getting_setting_name_of_Thread
{
	public static void main(String[] args) 
	{
		mythread t = new mythread();

		t.start();

		System.out.println("main method executed by Thread : " + Thread.currentThread().getName());// This line is executed by main Thread. 
	}
}

/*
o/p

main method executed by Thread : main
run() method executed by Thread-0
