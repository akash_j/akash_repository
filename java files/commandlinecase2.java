// A program to demonstrate comman line arguments

// case 2:

public  class commandlinecase2 
{
	public static void main(String[] args) 
	{
		String[] argh  = {"X" , "y", "Z"};

		args = argh;// here args is pointing to argh  = {"X" , "y", "Z"};

		for (String s : args ) 
		{
			System.out.println(s);
		}
	}
}

/* o/p 
 java commandlinecase2 A B C 
X
y
Z
 java commandlinecase2 A B
X
y
Z
 java commandlinecase2 
X
y
Z
*/