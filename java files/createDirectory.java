
// This program illustrates how to create directory in java 
import java.io.*;
public class createDirectory 
{
	public static void main(String[] args) throws Exception
	{
		File f = new File("akash1234"); // This line won't create any physical directory if the desired directory is not available

		System.out.println(f.isDirectory());

		System.out.println(f.exists());// returns false 

		f.mkdir(); // This line creates a physical directory named with akash123.

		System.out.println(f.exists());// returns true
	}
}