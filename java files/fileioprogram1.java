/*Write  a program  to create a directory named with abc 
in current working directory and create a ile named with  demo.txt in that directory*/

import java.io.*;

public class fileioprogram1 
{
	public static void main(String[] args)  throws Exception
	{
		File f  = new File ("foo");

			 f.mkdir(); // This line creates a directory named with xyz

		File f1 = new File (f , "demo.txt"); 

			 f1.createNewFile(); // This line creates a file named with demo.txt under directory named with xyz	

			 //System.out.println(f1.exists());// returns true if file demo.txt available in directory named with xyz

			 //System.out.println(f1.isFile()); // returns true if file object f1 is pointing to physical file

			 //System.out.println(f.isDirectory());// returns true if file object f is pointing to physical Directory

			  //f1.delete(); // This method is use to delete specified file or directory
			  //f.delete();
	}
}