/*
We can use BufferedWriter to wirte character data(text data) to the file.
To overcome the limitations of FileWriter we should go for BufferedWriter.

constructors:
1. BufferedWriter bw = new BufferedWriter(Writer w);
2. BufferedWriter bw = new BufferedWriter(Writer w, int buffersize);

Note:
BufferedWriter can't communicate with the file directly. It can communicate via some writer object only.

methods of BufferedWriter class
1. write(int num) :- To write single character to the file. 
	int ch is treated as unicode value.

2. write(char[] ch) --- To write an array of characters to the file.

3. write(String s) --- To write a String to the file.

4. flush() --- To give guarantee that total data including last character written properly to the file.This is very important method.

5. close() -- To close a FileWriter.

6. newLine() --- To insert a line separator.

Demo program to demonstrate BufferedWriter

*/
import  java.io.*;
public  class javaBufferedWriter
{
	public static void main(String[] args) throws IOException
	{
		FileWriter fw = new FileWriter("abc.txt");

		BufferedWriter bw = new BufferedWriter (fw);

		bw.newLine();
		
		bw.write(100);

		bw.newLine();

		char[]  ch = {'l' , 'm', 'n', 'o', 'p'};
		bw.write(ch);

		bw.newLine();

		bw.write("hello world java programming is wonderful..................");

		bw.newLine();

		bw.flush();

		//bw.close();
	}
}
/* Whenever we are closing  BufferedWriter underlying FileWriter will be closed automatically nad we are not require to close it 
explicitly.*/