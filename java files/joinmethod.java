/* prgram to demonstrate example of join() method 
every join() method throws InterruptedException, hence compulsory we should handle this exception either by using 
try-- catch block or by throws keyword otherwise we will get compile time error.
prototype of join() method :
1. public final void join() throws InterruptedException
2. public  final  void join(long ms) throws InterruptedException
3. public  final void join(long ms, int ns) throws InterruptedException

ms-- milliseconds and ns --- nano seconds 
*/

class mythread extends Thread
{
	public void run()
	{
		for (int i=1; i<=10 ; i++ ) //This loop will run till 10 milliseconds
		{
			System.out.println("seeta Thread");

			try
			{
				Thread.sleep(1000);
			}
			catch (InterruptedException e) 
			{
				
			}
		}
	}
}

public  class joinmethod
{
	public static void main(String[] args)  throws InterruptedException
	{
		mythread t  = new mythread();

		t.start();
		t.join(5000);// here main Thread is calling join() method  on child thread object hence main thread will enter into waiting state until completion of child thread.
		// main thread will wait till 5 seconds only.

		for (int i=1; i<=10 ; i++ ) 
		{
			System.out.println("Rama Thread");
			//Thread.sleep(1000);// sleep method always requires a parameter
			
		}
	}
}

/*note:-join() method and sleep() method throws InterruptedException hence we must handle this exception either by using 
try-- catch block or by throws keyword otherwise we will get compile time error.*/