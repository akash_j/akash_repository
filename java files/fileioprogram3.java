/* Program to print only names of files present in specified directory.
*/

import  java.io.*;
public  class fileioprogram3
{
	public static void main(String[] args) throws IOException
	{
		int count = 0;
		File f = new File ("/home/ajay123/Desktop/java files");

		String[]  s = f.list();

		for (String s1 : s ) 
		{
			File f1 = new File (f , s1);

			if (f1.isFile()) 
			{
				count++;
				System.out.println(" " + s1);
			}
		}

		System.out.println("The total number of files : " + count);

	}
}