/* Arrays are fixed in size .
Arrays can hold only homogenous data type of elements.
By using Arrays we can represent  huge number of elements by using single variable so that redability of code will be improved.

1-Dimension Array declaration

int[]    x; x is 1-Dimensional of type integer.
int     []x; x is 1-Dimensional of type integer.
int     x[]; x is 1-Dimensional of type integer.

All above 3 specificationa are valid.
Note :- At the time  of  declaration  we can 't  specify the size otherwise we will get compile tie error.

	e.g. int[5]  x;  ----  invalid
		 int[]	 x;  ----   valid



2-Dimensional Array declaration

int[][]     x; 
int 	[][]x;
int 	x[][];

use first 3

int[]	[]x;
int[]	x[];
int 	[]x[];

All above 2-Dimensional Arrays are valid.

special case :
int 	[]a,[]b; ---- compile time error.

note :- If we want to specify dimension before variable then this facility is only  available for 1st variable.
If we are trying to apply for remaining variables then we will get compile time error.

		e.g. int []a, []b, []c; ------ invalid.

