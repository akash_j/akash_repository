public class mainhiding
{
	public static void main(String[] args) {
		System.out.println("parent main method");
	}
}

 class B extends mainhiding
{
	public static void main(String[] args) {
		System.out.println("child main method");
	}
}// The above concept is not method overriding but method hiding
/*main() can  be oveloaded and inherited but it cannot be overrided 
instead of overriding method hiding is performed