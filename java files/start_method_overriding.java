/* program to demonstrate  overriding of start() method
If we override start() method of Thread class then our start() method will be executed just  like a normal method call 
and new Thread won't be created*/

class Mythread extends Thread
{
	public  void run()
	{
		System.out.println("hi i am run method");
	}

	public void start()
	{
		System.out.println("hi i am start method");
	}

	/*public void start(int a, int b) 
	{
		//overloading of start() method
		System.out.println("sum of start() method = " + (a+b) );
	}*/
}

class start_method_overriding
{
	public static void main(String[] args) 
	{
		Mythread t = new Mythread();

				 t.start();// no new Thread will be created here 
				 //t.start(10,20);

				 System.out.println("hi i am main method");
	}
}

//Both start() and run() method can be overloaded and can be overridden.