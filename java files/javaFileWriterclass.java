/* FileWriter class 
We can use  FileWriter object to write character data (text data) to file.

FileWriter constructors:
1. FileWriter fw = new FileWriter(String name);   String name ==  File name e.g. abc.txt 
2. FileWriter fw  = new FileWriter (File f);

	The above 2 constructors are meant for overriding existing data.
	Instead of overriding, if we want to perform append operation then we have use following 2 constructors.

3. FileWriter fw = new FileWriter(String name, boolean append);  boolean append =  true.
4. FileWriter fw  = new FileWriter (File f, boolean append);

note :- if the specified file is not already available then then all the above constructors will create that file.

methods of FileWriter class
1. write(int num) :- To write single character to the file. 
	int ch is treated as unicode value.

2. write(char[] ch) --- To write an array of characters to the file.

3. write(String s) --- To write a String to the file.

4. flush() --- To give guarantee that total data including last character written properly to the file.This is very important method.
5. close() -- To close a FileWriter.


Limitations of FileWriter class
The problem with the FileWriter is that we have to insert line separator (\n) manually which is varied from system to system.
It is very dificult for the programmer.

Demo program to demonstrate FileWriter
*/

import java.io.*;

public  class javaFileWriterclass
{
	public static void main(String[] args) throws IOException
	{
		  FileWriter fw  = new FileWriter("abc.txt"); 

/*1st method*/	    fw.write(100); //here corresponding character of 100 will be added to the file. 

					fw.write("\n");
		
		    	    fw.write(65);

					fw.write("\n"); // A line separator added manually.

/*2nd method*/		char[]  ch  = {'a', 'e', 'i', 'o', 'u', }; // An array of characters. 

					fw.write(ch);

					fw.write("\n");

/*3rd method*/		fw.write("Hello world \n I am learning java File IO.");

		            fw.write("\n");

					fw.write("akash jaiswal TYBSCIT");

/*4th method*/		fw.flush();// very importtant method 

/*5th method*/		fw.close(); // To close the FileWriter after completing write operation. 






	}
}