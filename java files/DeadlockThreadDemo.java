/* If a Thread calls join() method on same Thread itself then the program will be stuck (This is something like deadlock)
In this case Thread has to wait infinte amount of time

A program to illustrate deadlock situation 
*/

public  class DeadlockThreadDemo
{
	public static void main(String[] args) throws InterruptedException
	{
		  
			
		System.out.println("Hi there Deadlock situation will occur soon................... ");
		Thread.sleep(3000);
		     
		System.out.println("I am sleeping for 10 seconds");
		Thread.sleep(10000);

		System.out.println("Main Thread enters into  Deadlock state for 5 seconds"); 
		Thread.currentThread().join(5000);//here main Thread is calling join() method on its ownself.

		Thread.sleep(3000);
		System.out.println("Hi i am Main Thread and i am back out of Deadlock state");
	}
}