


class myrunnable implements Runnable //name of interface -- Runnable
{
	public void run()
	{
		for (int i=1; i<=10 ;i++ ) 
		{
			System.out.println("child Thread " + i);
		}
	}
}

class runnablecase_study
{
	public static void main(String[] args) 
	{
		myrunnable r = new myrunnable();

		Thread t1 = new Thread();

		Thread t2  = new Thread(r);

		// below is some case study discussed

		t1.start();// A new Thread will be created but Thread class run() method will be executed which has empty implementation.
		//hence we won't get any output.

		t2.start(); //A new Thread will be created which will invoke myrunnable class run() method

		//t2.run();A new thread won't be created and myrunnable class run() method will be execute just like normal method call.

		//r.start(); we will get compile time error bcoz myrunnable class doesn't have start capability

		//r.run(); no new Thread will be created and myrunnable run() method will be executed just like normal method


	}
}

/*This is another way to define a thread by extending Thread class but it is not recommended 

class mythread extends Thread
{
	public void run()
	{
		System.out.println("child Thread " );
	}
}

class ThreadDemo
{
	public static void main(String[] args) 
	{
		mythread t = new mythread();

		Thread  t1 = new Thread(t);

		t1.start();

		System.out.println("main Thread " );
	}
}
*/







































