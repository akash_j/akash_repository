//program to implement concept of multithreading by extending thread class

class Mythread extends Thread
{
	public void run()
	{
		for(int i=1; i<=10; i++)
		{
			System.out.println("child Thread");
			
		}

		
	}
}

public  class ThreadDemo1
{
	public static void main(String[] args) 
	{
		Mythread  t = new Mythread();

		
		          t.start();// Thread class Start() method is responsible to register thread with Thread schedulerT

		for (int i=1;i<=10 ;i++ )
		 {
			System.out.println("Main Thread");	
		 }

		//t.start(); If we are trying to start same Thread again then we will get runtime exception saying IllegalThreadStateException.s	
	}
}

 