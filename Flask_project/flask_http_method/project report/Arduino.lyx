#LyX 2.0 created this file. For more info see http://www.lyx.org/
\lyxformat 413
\begin_document
\begin_header
\textclass report
\use_default_options false
\maintain_unincluded_children false
\language english
\language_package default
\inputencoding auto
\fontencoding global
\font_roman default
\font_sans default
\font_typewriter default
\font_default_family default
\use_non_tex_fonts false
\font_sc false
\font_osf false
\font_sf_scale 100
\font_tt_scale 100

\graphics default
\default_output_format default
\output_sync 0
\bibtex_command default
\index_command default
\paperfontsize default
\spacing single
\use_hyperref false
\papersize default
\use_geometry false
\use_amsmath 1
\use_esint 1
\use_mhchem 1
\use_mathdots 0
\cite_engine natbib_authoryear
\use_bibtopic false
\use_indices false
\paperorientation portrait
\suppress_date false
\use_refstyle 0
\index Index
\shortcut idx
\color #008000
\end_index
\secnumdepth 3
\tocdepth 3
\paragraph_separation indent
\paragraph_indentation default
\quotes_language english
\papercolumns 1
\papersides 1
\paperpagestyle default
\tracking_changes false
\output_changes false
\html_math_output 0
\html_css_as_file 0
\html_be_strict false
\end_header

\begin_body

\begin_layout Chapter
Arduino
\end_layout

\begin_layout Standard
Arduino is an open-source electronics platform based on easy-to-use hardware
 and software.
 Arduino boards are able to read inputs - light on a sensor, a finger on
 a button, or a Twitter message - and turn it into an output - activating
 a motor, turning on an LED, publishing something online.
 You can tell your board what to do by sending a set of instructions to
 the microcontroller on the board.
 To do so you use the Arduino programming language (based on Wiring), and
 the Arduino Software (IDE), based on Processing.
 Over the years Arduino has been the brain of thousands of projects, from
 everyday objects to complex scientific instruments.
 A worldwide community of makers - students, hobbyists, artists, programmers,
 and professionals - has gathered around this open-source platform, their
 contributions have added up to an incredible amount of accessible knowledge
 that can be of great help to novices and experts alike.
 Arduino was born at the Ivrea Interaction Design Institute as an easy tool
 for fast prototyping, aimed at students without a background in electronics
 and programming.
 As soon as it reached a wider community, the Arduino board started changing
 to adapt to new needs and challenges, differentiating its offer from simple
 8-bit boards to products for IoT applications, wearable, 3D printing, and
 embedded environments.
 All Arduino boards are completely open-source, empowering users to build
 them independently and eventually adapt them to their particular needs.
 The software, too, is open-source, and it is growing through the contributions
 of users worldwide.
 
\end_layout

\begin_layout Standard
\begin_inset Graphics
	filename arduino.jpg
	scale 25

\end_inset


\end_layout

\begin_layout Standard

\series bold
Why Arduino?
\series default
 
\end_layout

\begin_layout Standard
Thanks to its simple and accessible user experience, Arduino has been used
 in thousands of different projects and applications.
 The Arduino software is easy-to-use for beginners, yet flexible enough
 for advanced users.
 It runs on Mac, Windows, and Linux.
 Teachers and students use it to build low cost scientific instruments,
 to prove chemistry and physics principles, or to get started with programming
 and robotics.
 Designers and architects build interactive prototypes, musicians and artists
 use it for installations and to experiment with new musical instruments.
 Makers, of course, use it to build many of the projects exhibited at the
 Maker Faire, for example.
 Arduino is a key tool to learn new things.
 Anyone - children, hobbyists, artists, programmers - can start tinkering
 just following the step by step instructions of a kit, or sharing ideas
 online with other members of the Arduino community.
 There are many other microcontrollers and microcontroller platforms available
 for physical computing.
 Parallax Basic Stamp, Netmedia's BX-24, Phidgets, MIT's Handyboard, and
 many others offer similar functionality.
 All of these tools take the messy details of microcontroller programming
 and wrap it up in an easy-to-use package.
 Arduino also simplifies the process of working with microcontrollers, but
 it offers some advantage for teachers, students, and interested amateurs
 over other systems: Inexpensive - Arduino boards are relatively inexpensive
 compared to other microcontroller platforms.
 The least expensive version of the Arduino module can be assembled by hand,
 and even the pre-assembled Arduino modules cost less than $50 Cross-platform
 - The Arduino Software (IDE) runs on Windows, Macintosh OSX, and Linux
 operating systems.
 Most microcontroller systems are limited to Windows.
 Simple, clear programming environment - The Arduino Software (IDE) is easy-to-u
se for beginners, yet flexible enough for advanced users to take advantage
 of as well.
 For teachers, it's conveniently based on the Processing programming environment
, so students learning to program in that environment will be familiar with
 how the Arduino IDE works.
 Open source and extensible software - The Arduino software is published
 as open source tools, available for extension by experienced programmers.
 The language can be expanded through C++ libraries, and people wanting
 to understand the technical details can make the leap from Arduino to the
 AVR C programming language on which it's based.
 Similarly, you can add AVR-C code directly into your Arduino programs if
 you want to.
 Open source and extensible hardware - The plans of the Arduino boards are
 published under a Creative Commons license, so experienced circuit designers
 can make their own version of the module, extending it and improving it.
 Even relatively inexperienced users can build the breadboard version of
 the module in order to understand how it works and save money.
\end_layout

\begin_layout Standard
\begin_inset Graphics
	filename arduino 2.jpg
	scale 25

\end_inset


\end_layout

\end_body
\end_document
