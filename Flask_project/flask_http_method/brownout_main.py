import sqlite3, time

# Open database connection
db = sqlite3.connect("./project_database.sqlite" )

# prepare a cursor object using cursor() method
cursor = db.cursor()

brownout_status = 1
thresh_hold = 5
reduce_power = 0


def get_brownout_status():
    query1 = ("select brownout_status from brownout_details ")
    cursor.execute(query1)
    brownout_status = cursor.fetchone()
    # return brownout_status
    brownout_status = int(brownout_status[0])
    # print type(brownout_status[0])
    # print brownout_status
    return brownout_status



def get_power_threshold():
    query1 = ("select power_threshold from brownout_details ")
    cursor.execute(query1)
    power_threshold  = cursor.fetchone()
    power_threshold  = int(power_threshold[0])
    return power_threshold


def update_status(var_app, var_stat):
	# Open database connection
	#db = sqlite3.connect("project_database.sqlite" )

	# prepare a cursor object using cursor() method
	#cursor = db.cursor()


	query = "update appliance_details set app_status = %d \
	 where app_name = '%s' " % (var_stat, var_app)

	#try:
	cursor.execute(query)
	print "updated status"
	db.commit()
	print var_app, var_stat

	#db.close()


def get_appliance_power(appname):
	
	# execute SQL query using execute() method.
	#cursor.execute("select * from table1 where app =  '%s' " % appname)
	query1 = "select * from appliance_details where app_name =  '%s' " % appname
	cursor.execute(query1)
	# Fetch a single row using fetchone() method.
	data = cursor.fetchone()
	#print data
	return data

def get_appliance_list():
	query1 = "select  app_name from appliance_details"
	cursor.execute(query1)
	data = cursor.fetchall()
	return data



def compute_total_power():
	# execute SQL query using execute() method.
	# query1 = ("""select app_power 
	# 			from appliance_preference""")	
	query1 = ("""select app_power 
				from appliance_details  where app_status = 1""")
	# print type(query1)
	cursor.execute(query1)
	rows = cursor.fetchall()
	print rows

	# power_list=[]
	sum_power = 0
	for power in rows:
		# print power
		power = int(power[0])
		# power_list.append(power)
		sum_power = sum_power + power

	# print power_list
	return sum_power

def reduce_current_power(): 
    # This function turns off the non - essential appliance which is of least priority  
	query1 = ("""select app_name from appliance_details
				where app_status = 1 and essential = 0 order by priority desc limit 1""")
	# print type(query1)
	cursor.execute(query1)
	rows = cursor.fetchone()
	print rows

	if rows == None: 
		print "Cannot turn off further appliance"
	else:
		appliance_to_OFF = rows[0]	
		print "Appliance to turn OFF: ", appliance_to_OFF	
		update_status(appliance_to_OFF, 0)




def brownout_control(total_power, power_threshold, brownout_status):
	#check for brownout status
	if brownout_status == 1:
		print "Brownout perod is ON"

		#compare current consumption with brownout threshold
		if total_power > power_threshold:
			print"Warning! power consumption is more than threshold"
			#turn OFF appliance if possible
			# reduce_power = 1
			reduce_current_power()

		elif total_power <= power_threshold:
			print "hurray!! power consumption is less than threshold"
			# reduce_power = 0	



while True:
	try:
		print "----------------" * 10
		
		brownout_status = get_brownout_status()
		print "Brownout status is %s" % brownout_status

		#check current consumption
		total_power = compute_total_power()
		print "Total power = %d " % total_power

		power_threshold = get_power_threshold()
		print "Power Threshold = %d " %power_threshold

		

		brownout_control(total_power, power_threshold, brownout_status)

		

		time.sleep(5)

	except KeyboardInterrupt:
		exit()


