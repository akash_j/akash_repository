'''
updating appliance status in database using function 
and post method along with json
'''

# -*- coding: utf-8 -*-
import sqlite3

from flask import Flask, redirect, url_for, request, json, jsonify, render_template
app = Flask(__name__)

#app_name = ""
#app_status = 0

@app.route('/', methods = ['POST', 'GET'])
def index():
    # Render template
    #return render_template('Jquery_appliance.html')
    return render_template('appliance.html')

def get_appliance_status(app_name):
    # query1 = "select * from appliance_details where app_name =  '%s' " % appname
    # cursor.execute(query1)
    # # Fetch a single row using fetchone() method.
    # data = cursor.fetchone()
    # #print data
    # return data
    db = sqlite3.connect("project_database.sqlite" )

  # prepare a cursor object using cursor() method
    cursor = db.cursor()

    query = "select app_status from appliance_details where app_name = ?"
    query_params = [app_name]  #params can be sequence/tuple or list
    
    #try:
    cursor.execute(query, query_params)
    rows = cursor.fetchall()

    db.commit()
    db.close()

    row_list = []
    for row in rows:
        print row
        row = row[0]
        row = str(row)
        row_list.append(row)
        print row_list

    app_status = row_list  
    # print app_status
    return app_status

@app.route('/appliance_status', methods = ['POST', 'GET'])
def appliance_status():
    if request.method == 'GET':
        return render_template("appliance.html")

    if request.method == 'POST':
        app_name = request.json['app_name']           
        print "appliance name", app_name
        app_name = str(app_name) # typecast unicode to string 

        app_status = get_appliance_status(app_name)
        app_status = app_status[0]
        print "appliance status", app_status

        response_data = {'app_status' : app_status}
        response_data = jsonify(response_data)
        return response_data


@app.route('/update_appliance', methods = ['POST', 'GET'])
def update_appliance():
   if request.method == 'POST':
      #app_name = request.form['app_name']
      #app_status = request.form['app_status']
      
      #app_name = request.json['app_name']
      #app_status = request.json['app_status']
      

      app_name = request.json['app_name'] # getting  value of app_name key value
      app_status = request.json['app_status']# getting  value of app_status key value
     
      
      '''
	      mydata = request.data 
	      print  mydata
	      print type(mydata)  #type is string 
	      #print mydata[0] 
	      #print mydata[1] 

	      mydata_json = json.loads(mydata) # converting string into dictionary
	      print mydata_json
	      print type(mydata_json)  # type is dictionary

	      app_name = mydata_json['app_name'] # storing key-value 
	      app_status = mydata_json['app_status']   # storing key-value 
      
       '''

      print app_name, app_status


      print "appliance name", app_name
      print "appliance status", app_status
      print "------------------------------------------------------------------------------------"
      #print type(app_name)
      #print type(app_status)
      
      app_name = str(app_name) # typecast unicode to string 
      app_status = int(app_status)# typecast unicode to int 
      #app_status = int(app_status)# converting string to integer
      #print type(app_name)
      #print type(app_status)
      

      update_status(app_name, app_status)
      return  jsonify(request.json)
   

def update_status(var_app, var_stat):
	# Open database connection
	db = sqlite3.connect("project_database.sqlite" )

	# prepare a cursor object using cursor() method
	cursor = db.cursor()


	query = "update appliance_details set app_status = %d \
	 where app_name = '%s' " % (var_stat, var_app)

	#try:
	cursor.execute(query)
	print "updated using post"
	db.commit()
	print var_app, var_stat

	db.close()



if __name__ == '__main__':
   app.run(debug = True,
    host = '0.0.0.0',
    port = 1234,
    threaded=True)










