#!/usr/bin/env python
#import serial

import sqlite3
from Arduino import Arduino
import time

board = Arduino('9600') #plugged in via USB, serial com at rate 9600


l1_pin = 8
l2_pin = 9
f1_pin = 10
f2_pin = 11

board.pinMode(l1_pin, "OUTPUT")
board.pinMode(l2_pin, "OUTPUT")
board.pinMode(f1_pin, "OUTPUT")
board.pinMode(f2_pin, "OUTPUT")

l1_on = 'l1_on'
l1_off ='l1_off'

l2_on = 'l2_on'
l2_off = 'l2_off'

f1_on = 'f1_on'
f1_off = 'f1_off'

f2_on = 'f2_on'
f2_off = 'f2_off'

locations=['/dev/ttyACM0' ,'/dev/ttyUSB0','/dev/ttyUSB1','/dev/ttyUSB2','/dev/ttyUSB3',
'/dev/ttyS0','/dev/ttyS1','/dev/ttyS2','/dev/ttyS3'] 



# for device in locations:  
#     try:  
#         print "Trying...",device
#         arduino = serial.Serial(device, 9600)
#         time.sleep(1)
#         print arduino.readline() 
#         break

#     except:  
#         print "Failed to connect on",device  



# Open database connection
db = sqlite3.connect("project_database.sqlite" )

# prepare a cursor object using cursor() method
cursor = db.cursor()


def get_appliance_status(appname):
    
    # execute SQL query using execute() method.
    #cursor.execute("select * from table1 where app =  '%s' " % appname)

    query1 = "select app_status from appliance_details where app_name =  '%s' " % appname
    cursor.execute(query1)
    # Fetch a single row using fetchone() method.
    data = cursor.fetchone()
    #print data
    return data

while True:
    #time.sleep(1)
    #---------------l1 begin ------------------
    l1_status = get_appliance_status('l1')
    #print l1_status[0] # printing appliance name
    #print l1_status[0] # printing appliance status


    if l1_status[0] == 1:
        print "l1 ONN"
        board.digitalWrite(l1_pin, "HIGH")
        #arduino.write(l1_on)
        #print arduino.readline()


    elif l1_status[0] == 0:
        print "l1 OFF"
        board.digitalWrite(l1_pin, "LOW")
        #arduino.write(l1_off)
        #print arduino.readline()

    time.sleep(1)

    #-------------l1 end ---------------------
    l2_status = get_appliance_status('l2')
    if l2_status[0] == 1:
        print "l2 ONN"
        board.digitalWrite(l2_pin, "HIGH")
        #arduino.write(l2_on)
        #print arduino.readline()

    elif l2_status[0] == 0:
        print "l2 OFF"
        board.digitalWrite(l2_pin, "LOW")
        #arduino.write(l2_off)
        #print arduino.readline()

    time.sleep(1)

    f1_status = get_appliance_status('f1')
    if f1_status[0] == 1:
        print "f1 ONN"
        board.digitalWrite(f1_pin, "HIGH")
        #arduino.write(f1_on)
        #print arduino.readline()

    elif f1_status[0] == 0:
        print "f1 OFF"
        board.digitalWrite(f1_pin, "LOW")

        #arduino.write(f1_off)
        #print arduino.readline()
    
    time.sleep(1)

    f2_status = get_appliance_status('f2')
    if f2_status[0] == 1:
        print "f2 ONN"
        board.digitalWrite(f2_pin, "HIGH")

        #arduino.write(f2_on)
        #print arduino.readline()

    elif f2_status[0] == 0:
        print "f2 OFF"
        board.digitalWrite(f2_pin, "LOW")

        #arduino.write(f2_off)
        #print arduino.readline()

    time.sleep(1)


# disconnect from server
db.close()
