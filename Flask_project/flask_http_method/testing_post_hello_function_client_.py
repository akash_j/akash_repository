# using python request moduel along with json to post user data.

import requests, json

myurl = "http://127.0.0.1:5000/hello"

#mypayload = "{\"first_name\": \"Zara\",  \"last_name\": \"khan\"}"
mypayload_dict = {"first_name": "Zara",  "last_name": "khan"}

#mypayload = str(mypayload_dict) # not applicable
mypayload = json.dumps(mypayload_dict)  # converting json dictionary to string


myheaders = {
    'content-type': "application/json",
   
    }

#response = requests.request("POST", url, data=payload, headers=headers)
response = requests.post(url = myurl, data=mypayload, headers=myheaders)


print(response.text)