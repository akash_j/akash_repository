# -*- coding: utf-8 -*-

from flask import Flask, redirect, url_for, request
app = Flask(__name__)

@app.route('/success/<name>')
def success(name):
   return 'welcome %s' % name

@app.route('/login',methods = ['POST', 'GET'])
def login():
   if request.method == 'POST':
      user = request.form['nm']
      print user
      #return redirect(url_for('success',name = user))
      return 'Hi i am post method'
   else:
      user = request.args.get('nm')
      #return redirect(url_for('success',name = user))
      print user
      return 'Hi i am get method'


def foo():
	print 'foo'
	return 'foo'

@app.route('/hello')
def hello():
    print "hello"
    myfoo = foo()
    return  myfoo

if __name__ == '__main__':
   app.run(debug = True)