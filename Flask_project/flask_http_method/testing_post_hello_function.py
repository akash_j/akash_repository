# using json to print first name and last name of user.

# -*- coding: utf-8 -*-

from flask import Flask, redirect, url_for, request, json, render_template, jsonify
app = Flask(__name__)


@app.route('/', methods = ['POST', 'GET'])
def index():
    # Render template
    return render_template('hello.html')


@app.route('/hello',methods = ['POST', 'GET'])
def login():
   if request.method == 'POST':
      #first = request.form['first_name']
      #last = request.form['last_name']

      first = request.json['first_name']
      last = request.json['last_name']


      '''
      mydata = request.data 
      print  mydata
      print type(mydata)  #type is string 
      #print mydata[0] 
      #print mydata[1] 

      mydata_json = json.loads(mydata) # converting string into dictionary
      print mydata_json
      print type(mydata_json)  # type is dictionary

      first= mydata_json['first_name'] # storing key-value 
      last= mydata_json['last_name']   # storing key-value 
      '''

      print first , last 

     # print "Welcome ", first, last
      #return redirect(url_for('success',name = user))
      hello(first, last)
      #return "Hi i am post method\n"
      return  jsonify(request.json)

   elif request.method == "GET":
   	return "Hi i  am get method\n"




def hello(first_hello, last_hello ):
	print "hello %s %s" % (first_hello, last_hello)



if __name__ == '__main__':
   app.run(debug = True)