import sqlite3, time

# Open database connection
db = sqlite3.connect("../project_database.sqlite" )

# prepare a cursor object using cursor() method
cursor = db.cursor()


def get_brownout_status():
    query1 = ("select brownout_status from brownout_details ")
    cursor.execute(query1)
    brownout_status = cursor.fetchone()
    # return brownout_status
    brownout_status = int(brownout_status[0])
    # print type(brownout_status[0])
    # print brownout_status
    return brownout_status





def get_power_threshold():
    query1 = ("select power_threshold from brownout_details ")
    cursor.execute(query1)
    power_threshold  = cursor.fetchone()
    power_threshold  = int(power_threshold[0])
    return power_threshold





power_threshold = get_power_threshold()
print power_threshold

brownout_status = get_brownout_status()
print brownout_status