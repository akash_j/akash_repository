import serial
import time

locations=['/dev/ttyACM0' ,'/dev/ttyUSB0','/dev/ttyUSB1','/dev/ttyUSB2','/dev/ttyUSB3',
'/dev/ttyS0','/dev/ttyS1','/dev/ttyS2','/dev/ttyS3']  
  
# global l1_on
# global l1_off

l1_on = 'l1 on'
l1_off ='l1 off'

for device in locations:  
	try:  
		print "Trying...",device
		arduino = serial.Serial(device, 9600) 
		break
	except:  
		print "Failed to connect on",device  
        
while True:
	try:
	    print arduino.readline()
	    #time.sleep(5)  
	    
	    # arduino.write('l1 on') 
	    arduino.write(l1_on)

	    print arduino.readline() 
	    time.sleep(1)
	    
	    # arduino.write('l1 off')
	    arduino.write(l1_off) 

	    print arduino.readline()
	    time.sleep(1)

	except KeyboardInterrupt  :  
         print "Failed to send!"
         exit() 
	