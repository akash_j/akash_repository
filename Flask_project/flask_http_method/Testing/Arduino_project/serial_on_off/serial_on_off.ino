//int ledPin = 13;  // choose the pin for the LED
int l1_pin = 8;
int l2_pin = 9;
int f1_pin = 10;
int f2_pin = 11; 
int val = 0;      // variable for reading the pin status
String msg = "";   // variable to hold data from serial


void setup() 
{
  pinMode(l1_pin, OUTPUT);
  pinMode(l2_pin, OUTPUT);
  pinMode(f1_pin, OUTPUT);
  pinMode(f2_pin, klOUTPUT);      // declare LED as output
  Serial.begin(9600);
  Serial.print("Program Initiated\n");  
}

void loop()
{
        // While data is sent over serial assign it to the msg
  while (Serial.available()>0)
  { 
    msg=Serial.readString();
    Serial.println(msg);
  }

  // -------------l1 begin --------------
  // Turn LED on/off if we recieve 'Y'/'N' over serial 
  if (msg== "l1_on") 
  {            
    digitalWrite(l1_pin, HIGH);  // turn LED ON
    //Serial.print("LED Activated\n");
    msg="";
    Serial.println(msg);
  }

   else if (msg== "l1_off")
    {
      digitalWrite(l1_pin, LOW); // turn LED OFF
      //Serial.println(msg);
      msg = "";
    }

  //-----------------l1 end -------------------
  
    
     if (msg== "l2_on") 
  {            
    digitalWrite(l2_pin, HIGH);  // turn LED ON
    //Serial.print("LED Activated\n");
    msg="";
    Serial.println(msg);
  }

   else if (msg== "l2_off")
    {
      digitalWrite(l2_pin, LOW); // turn LED OFF
      //Serial.println(msg);
      msg = "";
    }

     if (msg== "f1_on") 
  {            
    digitalWrite(f1_pin, HIGH);  // turn LED ON
    //Serial.print("LED Activated\n");
    msg="";
    Serial.println(msg);
  }

   else if (msg== "f1_off")
    {
      digitalWrite(f1_pin, LOW); // turn LED OFF
      //Serial.println(msg);
      msg = "";
    }



     if (msg== "f2_on") 
  {            
    digitalWrite(f2_pin, HIGH);  // turn LED ON
    //Serial.print("LED Activated\n");
    msg="";
    Serial.println(msg);
  }

   else if (msg== "f2_off")
    {
      digitalWrite(f2_pin, LOW); // turn LED OFF
      //Serial.println(msg);
      msg = "";
    }


}
