# -*- coding: utf-8 -*-

from flask import Flask, render_template
app = Flask(__name__)

@app.route('/appliance')
def index():
    return render_template('appliance.html')
    

if __name__ == "__main__":
    app.run()
   

