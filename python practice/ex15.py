
# in this program we are copying the content of file file1 to file 2
from sys import argv
from os.path import exists

script , from_file, to_file = argv

print "copying from %s to %s "% (from_file, to_file)

# here we are opening the file f1.txt, reading its content and storing it in indata variable.
in_file = open(from_file)
indata = in_file.read()

print "The input file is %d bytes long " % len(indata)# here we are printing the length of f1.txt interms of bytes.

print "Does the output file exists? %s" % exists(to_file)# this line wll return true if file f2.txt exists.
print "Ready, hit ENTER to continue, CTRL-C to abort."

raw_input()

# here we are opening the  file f2.txt in write mode and copying the content in it.
out_file = open(to_file, 'w')
out_file.write(indata)

print "Alright, all done "

out_file.close()
in_file.close()
