from sys import argv #sys is package name or module or library and we are importing argv feature into our program 
script, filename = argv	
print "We are going to erase %s " % filename
print "If you don't want that hit CTRL-C (^C)"
print "If you do want that, hit Return"

raw_input("?")

print "opening the file..."
target = open (filename, 'w')# here we are opening the file in write mode
# bydefault open() method opens the file in read mode

print "truncating the file. goodbye!!"
target.truncate() # truncate() method empties the file with existing content
	
print "Now i am going to ask you for three lines"
line1 = raw_input("line 1: ")
line2 = raw_input("line 2: ")
line3 = raw_input("line 3: ")

print "I am going to write these to the file"

target.write(line1)# writing line 1 into file
target.write("\n")

target.write(line2)# writing line 3 into file
target.write("\n")

target.write(line3)# writing line 3 into file
target.write("\n")	

print "And finally we close it"
target.close()	# closing the file	
