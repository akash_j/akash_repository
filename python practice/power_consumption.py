a1 = 1
a2 = 0
a3 = 0
a4 = 1

p1 = 25
p2 = 25
p3 = 50
p4 = 50

thresh_hold = 100

power1 = a1 * p1
power2 = a2 * p2
power3 = a3 * p3
power4 = a4 * p4


print "power 1 = %d "%power1
print "power2 = %d " %power2
print "power3 = %d " %power3
print "power4 = %d " %power4

total_power = a1*p1 + a2*p2 + a3*p3 + a4*p4

print "Total power = %d " % total_power

if total_power > thresh_hold:
	print"Warning! power consumption is more than threshold"

elif total_power <= thresh_hold:
	print "hurray!! power consumption is less than threshold"