#python builtin modules
print (pow(2,3))	# using builtin function pow to get the cube of 2
print  (pow(3,0.5))
print  (pow(10,10))

print (dir(__builtins__)) # A command to get all builtin functions
print  (abs(.7321321)) # To get the absoulute value of the argument

print (len('hello')) # A builtin function to get the length of string
print (len('h '))

print (help (max)) # To know the functionality of the function or what the function does
print (help(abs)) 

print (max(5,7,4,8,9,6,2)) # returns the maximum number inside parantheses

import math
print (math.sqrt(64)) 

