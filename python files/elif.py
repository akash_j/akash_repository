n = input("Enter a name ")

if n == "Akash" :
    print ("Entered name is ", n) # this  programm is very imporant

elif n=="Karan" :
    print ("Entered name is ", n)

elif n=="Aman":
    print ("Entered name is ", n)

elif n == "Ajay":
    print ("Entered name is ", n)
else :
    print ("Entered name ", n, " is  not valid")
#The above and below program is of multiple if condition


name = "animal"
animalname = "dog"

if name == "animal":  # if this line returns false then control will be transferred to else statement, it won't enter in nested if

    if animalname == "dog" :
        print ("Valid animal")
    else :
        print ("animalname is invalid")
    print ("Name entered is animal")
else :
    print ("The name entered is not valid")

# the or operator always acts on true condition
# The and operator always acts on false condition 
