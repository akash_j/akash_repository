>>> str = 'this is string'
>>> str
'this is string'
>>> print (str)
this is string
>>> print (str)
this is string
>>> print ('hi') , print ('hello')
hi
hello
(None, None)
>>> print ('hi') , print ('hello')

hi
hello
(None, None)
>>> print ('hi');print ('hello')
hi
hello
>>> str2  =  '''this
is my world
of programming'''
>>> str2
'this\nis my world\nof programming'
>>> print (str2)
this
is my world
of programming
>>> a = b = c = 100 # Assigning single value to multiple variables
