# A demonstration for function in python

def hello (name):
    print ('hello ', name)

#input ('press enter to exit')
# def is reserved keyword
# hello is user defined function name
# name is an argument passed


def cal(x,y):
    print ('The addittion = ',x+y)

    print ('The subtraction = ',x - y)

    print ('The division = ',x / y)

    print ('The multiplication = ',x*y)

    print ('The exponential = ',pow(x,y))

    '''a = 1
    while a <= 10:
        print (a)
        a = a + 1'''  # following statement can be included in loop 

# The above program is just a basic calculation programm

#White space doesn 't matter anywhere in programm
def marks (name , num ):    # It is equivalent to   marks(name = 'akash' , nm = 20)
        print (name, "scored" , num, 'marks')


             
































