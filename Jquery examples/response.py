# -*- coding: utf-8 -*-

from flask import Flask, render_template, request
app = Flask(__name__)

@app.route('/response', methods=['POST','GET'])
def index():
    if request.method == 'POST':
	     return 'thank you for response';
    elif request.method== 'GET':
         return render_template('Jquery_ajax_reponse.html')
    

if __name__ == "__main__":
    app.run(debug = "True")

   

