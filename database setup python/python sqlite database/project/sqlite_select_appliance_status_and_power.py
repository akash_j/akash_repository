'''
- Defining a variable for the query and passing that variable to cursor.excute()  function 
inside get_appliance_status() function
- retriveing appliance status  by passing variable as parameter to query
- return in function
'''

import sqlite3

# Open database connection
db = sqlite3.connect("project_database.sqlite" )

# prepare a cursor object using cursor() method
cursor = db.cursor()

#myapp = 'l1'
#myquery  = "select * from table1 where app =  '%s' " % appname


def get_appliance_status(appname):
	
	# execute SQL query using execute() method.
	#cursor.execute("select * from table1 where app =  '%s' " % appname)
	query1 = "select * from appliance_status where app =  '%s' " % appname
	cursor.execute(query1)
	# Fetch a single row using fetchone() method.
	data = cursor.fetchone()
	#print data
	return data


def get_all_appliance_status():
	query1 = "select * from appliance_status "
	cursor.execute(query1)
	# Fetch a single row using fetchone() method.
	data = cursor.fetchall()
	#print data
	return data

def get_appliance_power(appname):
	
	# execute SQL query using execute() method.
	#cursor.execute("select * from table1 where app =  '%s' " % appname)
	query1 = "select * from appliance_power where app =  '%s' " % appname
	cursor.execute(query1)
	# Fetch a single row using fetchone() method.
	data = cursor.fetchone()
	#print data
	return data
	


data1 = get_appliance_status('l1')
print data1

data2 = get_appliance_status('l2')
print data2

getpower1 = get_appliance_power('l1')
print getpower1

getpower2 = get_appliance_power('l2')
print getpower2

all_data = get_all_appliance_status()
print all_data

# disconnect from server
db.close()
