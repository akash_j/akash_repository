'''
Defining a variable for the query and passing that variable to cursor.excute()  function 
inside get_appliance_status() function
retriveing appliance status  by passing variable as parameter to query
'''

import MySQLdb

# Open database connection
db = MySQLdb.connect("localhost","root","seil123","db1" )

# prepare a cursor object using cursor() method
cursor = db.cursor()

#myapp = 'l1'
#myquery  = "select * from table1 where app =  '%s' " % appname


def get_appliance_status(appname):
	
	# execute SQL query using execute() method.
	#cursor.execute("select * from table1 where app =  '%s' " % appname)
	query1 = "select * from table1 where app =  '%s' " % appname
	cursor.execute(query1)
	# Fetch a single row using fetchone() method.
	data = cursor.fetchone()
	print data
	



get_appliance_status('l1')
get_appliance_status('l2')


# disconnect from server
db.close()
