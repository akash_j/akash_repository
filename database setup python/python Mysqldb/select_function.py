'''
simple Mysqldb connection using function
'''

import MySQLdb

# Open database connection
db = MySQLdb.connect("localhost","root","seil123","db1" )

# prepare a cursor object using cursor() method
cursor = db.cursor()

def myselect():
	# execute SQL query using execute() method.
	cursor.execute("SELECT VERSION()")
	# Fetch a single row using fetchone() method.
	data = cursor.fetchone()
	print "Database version : %s " % data
	



myselect()
myselect()


# disconnect from server
db.close()
