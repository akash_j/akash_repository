'''
Defining a variable for the query and passing that variable to cursor.excute()  function
'''

import MySQLdb

# Open database connection
db = MySQLdb.connect("localhost","root","seil123","db1" )

# prepare a cursor object using cursor() method
cursor = db.cursor()


myquery = "SELECT VERSION()"

# execute SQL query using execute() method.
cursor.execute(myquery)

# Fetch a single row using fetchone() method.
data = cursor.fetchone()

print "Database version : %s " % data

# disconnect from server
db.close()