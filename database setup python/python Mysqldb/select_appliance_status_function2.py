'''
Defining a variable for the query and passing that variable to cursor.excute()  function 
inside myselect() function
retriveing appliance status  by passing variable as parameter to query
'''

import MySQLdb

# Open database connection
db = MySQLdb.connect("localhost","root","seil123","db1" )

# prepare a cursor object using cursor() method
cursor = db.cursor()

myapp = 'l1'
myquery  = "select * from table1 where app =  '%s' " % myapp

def myselect():
	# execute SQL query using execute() method.
	cursor.execute(myquery)
	# Fetch a single row using fetchone() method.
	data = cursor.fetchone()
	print data
	



myselect()
myselect()


# disconnect from server
db.close()
