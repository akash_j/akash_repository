'''
Defining a variable for the query and passing that variable to cursor.excute()  function 
retrieving appliance status 
'''

import MySQLdb

# Open database connection
db = MySQLdb.connect("localhost","root","seil123","db1" )

# prepare a cursor object using cursor() method
cursor = db.cursor()


myquery = "select * from table1 where app = 'l1' "

# execute SQL query using execute() method.
cursor.execute(myquery)

# Fetch a single row using fetchone() method.
data = cursor.fetchone()

print data

# disconnect from server
db.close()