"""
model.py
"""

import sqlite3

def connect_db():
    return sqlite3.connect("tipsy.db")



def new_user(db, email, password, name):
    c = db.cursor()
    query = """INSERT INTO Users VALUES (NULL, ?, ?, ?)"""
    result = c.execute(query, (email, password, name))
    db.commit()
    return result.lastrowid





def new_task(db, title, user_id):
    c = db.cursor()
    query = """INSERT INTO Tasks VALUES (NULL,?, ?)"""
    result =  c.execute(query, (title, user_id))
    db.commit()
    return result.lastrowid





def get_tasks(db, user_id):
    c = db.cursor()
    if user_id:
        query = """SELECT * from Tasks WHERE user_id = ?"""
        c.execute(query, (user_id,))
    else:
        query = """SELECT * from Tasks"""
        c.execute(query)
    tasks = []
    rows = c.fetchall()
    for row in rows:
        task = dict(zip(["id", "title", "created_at", "completed_at", "user_id"], row))
        tasks.append(task)

    return tasks


def authenticate(db, email, password):
    c = db.cursor()
    query = """SELECT * from Users WHERE email=? AND password=?"""
    c.execute(query, (email, password))
    result = c.fetchone()
    if result:
        fields = ["id", "email", "password", "username"]
        return dict(zip(fields, result))

    return None



def get_user(db, user_id):
    c = db.cursor()
    query = """SELECT * from Users WHERE id=? """
    c.execute(query, (user_id))
    result = c.fetchone()
    if result:
        fields = ["id", "email", "password", "username"]
        # return dict(zip(fields, result))
        return "Thnak you"
        total_users =  dict(zip(fields, result))
        print total_users


    return None








# If you use our implementation, make sure to
# look up the zip function

# connect_db()