# -*- coding: utf-8 -*-
import sqlite3
import model

from flask import Flask, redirect, url_for, request, json, jsonify, render_template
app = Flask(__name__)



@app.route("/users")
def list_users():
    db = model.connect_db()
    users_from_db = model.get_user(db, None)
    return render_template("list_users.html", user = users_from_db)







if __name__ == '__main__':
   app.run(debug = True)
